<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IngredientesController;
use App\Http\Controllers\TipoIngredientesController;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'api','prefix' => 'auth'], 
    function ($router) {                
        Route::post('/login', [AuthController::class, 'login']);
        Route::post('/register', [AuthController::class, 'register']);
        Route::post('/logout', [AuthController::class, 'logout']);
        Route::post('/refresh', [AuthController::class, 'refresh']);
        Route::get('/user-profile', [AuthController::class, 'userProfile']);    
    });


Route::middleware('auth:api')->group(function () {
    
    Route::get('/ingrediente', [IngredientesController::class, 'index']);
    Route::post('/ingrediente', [IngredientesController::class, 'store']);
    Route::get('/ingrediente/{ingredientes}', [IngredientesController::class, 'show']);
    Route::put('/ingrediente/{ingredientes}', [IngredientesController::class, 'update']);
    Route::delete('/ingrediente/{ingredientes}', [IngredientesController::class, 'destroy']);

    Route::get('/tipo', [TipoIngredientesController::class, 'index']);
    Route::post('/tipo', [TipoIngredientesController::class, 'store']);
    Route::get('/tipo/{tipoIngredientes}', [TipoIngredientesController::class, 'show']);
    Route::put('/tipo/{tipoIngredientes}', [TipoIngredientesController::class, 'update']);
    Route::delete('/tipo/{tipoIngredientes}', [TipoIngredientesController::class, 'destroy']);

    Route::get('/cliente', [ClienteController::class, 'index']);
    Route::post('/cliente', [ClienteController::class, 'store']);
    Route::get('/cliente/{cliente}', [ClienteController::class, 'show']);
    Route::put('/cliente/{cliente}', [ClienteController::class, 'update']);
    Route::delete('/cliente/{cliente}', [ClienteController::class, 'destroy']);
});

