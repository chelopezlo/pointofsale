<?php

namespace App\Http\Controllers;

use App\Models\TipoIngredientes;
use Illuminate\Http\Request;

class TipoIngredientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return TipoIngredientes::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tipoIngrediente = new TipoIngredientes();
        $tipoIngrediente->nombre = $request->get('nombre');
        $tipoIngrediente->producto = $request->get('producto');
        $tipoIngrediente->descripcion = $request->get('descripcion');
        $tipoIngrediente->save();
        return $tipoIngrediente;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TipoIngredientes  $tipoIngredientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoIngredientes $tipoIngredientes)
    {
        $tipoIngredientes->update($request->all());

        return $tipoIngredientes;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TipoIngredientes  $tipoIngredientes
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoIngredientes $tipoIngredientes)
    {
        $tipoIngredientes->delete();
        return response(['message' => 'Deleted']);
    }

    /**
     * Display the specified resource from storage.
     *
     * @param  \App\Models\TipoIngredientes  $tipoIngredientes
     * @return \Illuminate\Http\Response
     */
    public function show(TipoIngredientes $tipoIngredientes)
    {
        return $tipoIngredientes;
    }


}
