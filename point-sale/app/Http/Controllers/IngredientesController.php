<?php

namespace App\Http\Controllers;

use App\Models\Ingredientes;
use Illuminate\Http\Request;

class IngredientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Ingredientes::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ingredientes = new Ingredientes();
        $ingredientes->nombre = $request->get('nombre');
        $ingredientes->tipo = $request->get('tipo');
        $ingredientes->descripcion = $request->get('descripcion');
        $ingredientes->categoria = $request->get('categoria');
        $ingredientes->gramaje = $request->get('gramaje');
        $ingredientes->save();
        return $ingredientes;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ingredientes  $ingredientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ingredientes $ingredientes)
    {
        $data = $ingredientes->update($request->all());
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ingredientes  $ingredientes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ingredientes $ingredientes)
    {
        $ingredientes->delete();
        return response(['message' => 'Deleted']);
    }

    /**
     * Show the specified resource from storage.
     *
     * @param  \App\Models\Ingredientes  $ingredientes
     * @return \Illuminate\Http\Response
     */
    public function show(Ingredientes $ingredientes)
    {
        //$data = Ingredientes::with('tipoIngrediente')->find($ingredientes->id);
        return $ingredientes;
    }
}
