<?php

namespace App\Models;

use Facade\FlareClient\Http\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Cliente extends Eloquent
{
    use HasFactory;
    protected $connection = 'mongodb';
    protected $collection = 'clientes';
    protected $primarykey = 'id';
    protected $fillable = [
        'nombre',
        'fono',
        'direccion'
    ];
}
