<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Ingredientes extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'ingredientes';
    use HasFactory;


     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'tipo',
        'descripcion',
        'categoria',
        'gramaje'
    ];

    //protected $appends = ['TipoIngredientes'];
    protected $with = ['tipoIngrediente'];

    protected $casts = [
        'tipo' => 'string',
    ];

    public function tipoIngrediente()
    {
        return $this->belongsTo(\App\Models\TipoIngredientes::class, 'tipo');
    }

    public function getTipoIngredientesAttribute()
    {
        return $this->tipoIngrediente;
    }
}
