<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TipoIngredientes extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'tipoingredientes';

    use HasFactory;

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'producto',
        'descripcion',
    ];

    public function ingredientes()
    {
        return $this->hasMany(\App\Models\Ingredientes::class, 'id', 'tipo');
    }
}
