<?php

namespace Database\Factories;

use App\Models\TipoIngredientes;
use Illuminate\Database\Eloquent\Factories\Factory;

class TipoIngredientesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TipoIngredientes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
