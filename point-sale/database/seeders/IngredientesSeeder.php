<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Ingredientes;

class IngredientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ingredientes::truncate();

        $faker = \Faker\Factory::create();

        Ingredientes::create([
            'nombre' => 'palta',
            'tipo' => 'test',
            'descripcion' => 'test descripcion',
            'categoria' => 'test categoria',
            'gramaje' => 'test gramaje',
        ]);

        // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 10; $i++) {
            Ingredientes::create([
                'nombre' => 'nombre'.$i,
                'tipo' => 'tipo'.$i,
                'descripcion' => 'descripcion'.$i,
                'categoria' => 'categoria'.$i,
                'gramaje' => 'gramaje'.$i,
            ]);
        }
    }
}
