<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserSeeder::class);        
        $this->call(TipoIngredientesSeeder::class);
        $this->call(IngredientesSeeder::class);
        $this->call(ClienteSeeder::class);
    }
}
