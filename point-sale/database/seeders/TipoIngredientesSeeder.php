<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\TipoIngredientes;

class TipoIngredientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoIngredientes::truncate();

        $faker = \Faker\Factory::create();

        TipoIngredientes::create([
            'nombre' => 'vegetal',
            'producto' => 'test',
            'descripcion' => 'test descripcion',
        ]);

        // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 10; $i++) {
            TipoIngredientes::create([
                'nombre' => 'nombre'.$i,
                'producto' => 'producto'.$i,
                'descripcion' => 'descripcion'.$i,
            ]);
        }
    }
}
