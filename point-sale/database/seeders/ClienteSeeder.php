<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Cliente;

class ClienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cliente::truncate();

        $faker = \Faker\Factory::create();

        Cliente::create([
            'nombre' => 'usuario 1',
            'fono' => '23341',
            'direccion' => 'Esquina 1'
        ]);

        // And now let's generate a few dozen users for our app:
        for ($i = 0; $i < 10; $i++) {
            Cliente::create([
                'nombre' => 'nombre'.$i,
                'fono' => 'fono'.$i,
                'direccion' => 'direccion'.$i,
            ]);
        }
    }
}
