import * as express from 'express';
import * as cors from "cors";
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import {Routes} from './routes/ComandasRoutes';

class App {

    public app: express.Application;
    public routes : Routes = new Routes();
    public mongoUrl: string = 'mongodb://localhost/comandas';
    
    constructor() {
        this.app = express();
        this.config();
        this.routes.routes(this.app);
        this.mongoSetup();
    }

    private config(): void {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({
            extended: false
        }));
        this.app.use(cors());
    }

    private mongoSetup() : void {
        mongoose.Promise = global.Promise;
        mongoose.connect(this.mongoUrl);
    }
}

export default new App().app;