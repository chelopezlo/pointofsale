import * as mongoose from 'mongoose';
import {Cliente} from '../models/Cliente';
import {Request, Response} from 'express';

export class ClienteController{

    public create(req: Request, res: Response){
        let newTipo = new Cliente(req.body);

        newTipo.save((err, doc) => {
            if(err){
                res.status(500).send(err);
            }
            res.json(doc);
        });
    }

    public getAll(req: Request, res: Response)
    {
        Cliente.find({}, (err, doc) => {
            if(err){
                res.status(500).send(err);
            }
            res.json(doc);
        });
    }

    public getOne(req: Request, res: Response)
    {
        Cliente.findById(req.params.id, (err, doc) => {
            if(err){
                res.status(500).send(err);
            }
            res.json(doc);
        });
    }

    /**
     * updateCliente
     */
    public update(req: Request, res: Response) {
        Cliente.findOneAndUpdate({_id: req.params.id},
            req.body,
            {new: true},
            (err, doc) => {
                if(err){
                    res.status(500).send(err);
                }
                res.json(doc);
            });
    }

    /**
     * deleteCliente
     */
    public delete(req: Request, res: Response) {
        Cliente.remove({_id: req.params.id},
            (err, doc) => {
                if(err){
                    res.status(500).send(err);
                }
                res.json({ message: 'Se ha removido el tipo de ingrediente correctamente.'});
            }
        );
    }
}