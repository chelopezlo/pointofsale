import {TipoIngrediente} from '../models/TipoIngrediente';
import {Request, Response} from 'express';

export class TipoIngredienteController{

    public create(req: Request, res: Response){
        let newTipo = new TipoIngrediente(req.body);

        newTipo.save((err, doc) => {
            if(err){
                res.status(500).send(err);
            }
            res.json(doc);
        });
    }

    public getAll(req: Request, res: Response)
    {
        TipoIngrediente.find({}, (err, doc) => {
            if(err){
                res.status(500).send(err);
            }
            res.json(doc);
        });
    }

    public getOne(req: Request, res: Response)
    {
        TipoIngrediente.findById(req.params.id, (err, doc) => {
            if(err){
                res.status(500).send(err);
            }
            res.json(doc);
        });
    }

    /**
     * updateTipoIngrediente
     */
    public update(req: Request, res: Response) {
        TipoIngrediente.findOneAndUpdate({_id: req.params.id},
            req.body,
            {new: true},
            (err, doc) => {
                if(err){
                    res.status(500).send(err);
                }
                res.json(doc);
            });
    }

    /**
     * deleteTipoIngrediente
     */
    public delete(req: Request, res: Response) {
        TipoIngrediente.remove({_id: req.params.id},
            (err, doc) => {
                if(err){
                    res.status(500).send(err);
                }
                res.json({ message: 'Se ha removido el tipo de ingrediente correctamente.'});
            }
        );
    }
}