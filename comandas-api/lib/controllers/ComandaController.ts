import {Comanda} from '../models/Comanda';
import {DetalleComanda} from '../models/DetalleComanda';
import {Request, Response} from 'express';
import { Ingrediente } from 'models/Ingrediente';

export class ComandaController{

    public create(req: Request, res: Response){
        let newTipo = new Ingrediente(req.body);

        newTipo.save((err, doc) => {
            if(err){
                res.status(500).send(err);
            }
            res.json(doc);
        });
    }

    public getAll(req: Request, res: Response)
    {
        Ingrediente
            .find()
            .populate('tipo')
            .exec((err, doc) => {
                if(err){
                    res.status(500).send(err);
                }
                res.json(doc);
            });
    }

    public getOne(req: Request, res: Response)
    {
        Ingrediente
            .findById(req.params.id)
            .populate('tipo')
            .exec((err, doc) => {
                if(err){
                    res.status(500).send(err);
                }
                res.json(doc);
            });
    }

    /**
     * updateIngrediente
     */
    public update(req: Request, res: Response) {
        Ingrediente.findOneAndUpdate({_id: req.params.id},
            req.body,
            {new: true},
            (err, doc) => {
                if(err){
                    res.status(500).send(err);
                }
                res.json(doc);
            });
    }

    /**
     * deleteIngrediente
     */
    public delete(req: Request, res: Response) {
        Ingrediente.remove({_id: req.params.id},
            (err) => {
                if(err){
                    res.status(500).send(err);
                }
                res.json({ message: 'Se ha removido el ingrediente correctamente.'});
            }
        );
    }
}