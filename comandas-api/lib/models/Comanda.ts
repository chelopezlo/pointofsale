import mongoose, { model, Schema, Types, Document } from "mongoose";
import {Cliente, IClienteDoc} from './Cliente'

type ID = Types.ObjectId

export enum TipoDespacho {
	Retiro = 0,
	Domicilio = 1
};

export enum TipoPago {
	Efectivo = 0,
	Transferencia = 1
};

export enum Estado {
    Abierto = 0,
    EnProceso = 1,
    Envasado = 2,
    EnReparto = 3,
    Entregado = 4
};


interface IComanda{
    titulo:string;
    fecha:string;
    hora_inicio?:string;
    hora_prevista?:string;
    cliente:ID | IClienteDoc;
    tipo_despacho:number;
    direccion_despacho?:string;
    referencia_despacho?:string;
    valor?:number;
    tipo_pago?:number;
    estado_pago?:number;
    estado:number;
}

interface IComandaDoc extends IComanda, Document {}

const ComandaSchemaFields : Record<keyof IComanda, any> = {
    titulo: {
        type: String
    },
    fecha: {
        type: String,
        required: true
    },
    hora_inicio: {
        type: String,
    },
    hora_prevista:{
        type: String,
    },
    cliente: {
        type: Schema.Types.ObjectId,
        ref: 'Cliente'
    },
    tipo_despacho:{
        type: String, 
		enum: Object.values(TipoDespacho) 
    },
    direccion_despacho: {
        type: String
    },
    referencia_despacho: {
        type: String
    },
    valor: {
        type: Number
    },
    tipo_pago: {
        type: String, 
		enum: Object.values(TipoPago) 
    },
    estado_pago: {
        type: Number 
    },
    estado: {
        type: String, 
		enum: Object.values(Estado) 
    }
}

const ComandaSchema = new Schema(ComandaSchemaFields);

const Comanda = model<IComandaDoc>('Comanda', ComandaSchema);

export { Comanda, IComanda, IComandaDoc }