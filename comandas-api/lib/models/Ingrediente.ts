import { model, Schema, Types, Document } from "mongoose";
import { ITipoIngredienteDoc } from './TipoIngrediente'

export enum Categoria {
	Basico = 1,
	Premium = 2,
	NA = 9
};

type ID = Types.ObjectId

interface IIngrediente{
    nombre: string;
    tipo: ID | ITipoIngredienteDoc;
    descripcion?: string;
    categoria: string;
    gramaje?: number;
}

interface IIngredienteDoc extends IIngrediente, Document {}

const IngredienteSchemaField : Record<keyof IIngrediente, any> = {
	nombre: {
		type: String,
		trim: true,
		required: true,
	},
	tipo: {
		type: Schema.Types.ObjectId,
		ref: 'TipoIngrediente',
		required: true,
	},
	descripcion: {
		trim: true,
		type: String
	},
	categoria: { 
		type: String, 
		enum: Object.values(Categoria) 
	},
	gramaje: {
		type: Number
	}
}

const IngredienteSchema = new Schema(IngredienteSchemaField);

const Ingrediente = model<IIngredienteDoc>('Ingrediente', IngredienteSchema);


export { Ingrediente, IIngrediente, IIngredienteDoc }
