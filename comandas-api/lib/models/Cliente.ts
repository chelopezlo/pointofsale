import { Schema, Document, model } from 'mongoose';

interface ICliente {
    nombre_cliente:string;
    fono:string;
}

interface IClienteDoc extends ICliente, Document {}

const ClienteSchemaField : Record<keyof ICliente, any> = {
    nombre_cliente: {
        type: String,
        required: true,        
    },
    fono: {
        type: String,
    }
}

const ClienteSchema = new Schema(ClienteSchemaField);

const Cliente = model<IClienteDoc>('Cliente', ClienteSchema);

export { Cliente, IClienteDoc, ICliente}