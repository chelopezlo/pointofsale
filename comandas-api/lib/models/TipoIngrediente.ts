import mongoose, { model, Schema, Document } from 'mongoose';

interface ITipoIngrediente{
    nombre:string;
    producto:string;
    descripcion?:string;
}

interface ITipoIngredienteDoc extends ITipoIngrediente, Document {}

const TipoIngredienteSchemaField : Record<keyof ITipoIngrediente, any> = {
	nombre: {
		type: String,
		trim: true,
		required: true,
	},
	producto: {
		type: String,
		trim: true,
		required: true,
	},
	descripcion: {
		type: String,
		trim: true
	},
}

const TipoIngredienteSchema = new Schema(TipoIngredienteSchemaField);

const TipoIngrediente = model<ITipoIngredienteDoc>('TipoIngrediente', TipoIngredienteSchema);

export { TipoIngrediente,  ITipoIngrediente, ITipoIngredienteDoc }
