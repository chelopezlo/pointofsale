import mongoose, { model, Schema, Types, Document } from "mongoose";
import { Ingrediente, IIngredienteDoc } from './Ingrediente'

export enum Categoria {
	Sushi = 0,
	Pizza = 1,
	Otro = 9
};

type ID = Types.ObjectId

interface IProducto{
    nombre: string;
    ingredientes?: ID[] | IIngredienteDoc[];
    descripcion?: string;
    categoria: string;
    valorPropuesto: number;
}

interface IProductoDoc extends IProducto, Document {}

const ProductoSchemaField : Record<keyof IProducto, any> = {
    nombre: {
		type: String,
		trim: true,
		required: true,
    },
    ingredientes: [{
        type: Schema.Types.ObjectId,
		ref: 'Ingrediente',
    }],
    descripcion: {
        trim: true,
		type: String
    },
    categoria: {
        type: String, 
		enum: Object.values(Categoria) 
    },
    valorPropuesto: {
        type: Number
    }
}

const ProductoSchema = new Schema(ProductoSchemaField);

const Producto = model<IProductoDoc>('Producto', ProductoSchema);

export { Producto, IProducto, IProductoDoc }