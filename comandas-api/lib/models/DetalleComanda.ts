import { model, Schema, Document } from "mongoose";
import { Producto, IProductoDoc } from './Producto'

interface IDetalleComanda {
    comanda: string;
    producto: string;
    observacion: string;
}

interface IDetalleComandaDoc extends IDetalleComanda, Document {}

const DetalleComandaSchemaField : Record<keyof IDetalleComanda, any> = {
    comanda: {
        type: Schema.Types.ObjectId,
        ref: 'Comanda',
        required: true
    },
    producto: {
        type: Schema.Types.ObjectId,
        ref: 'Producto',
        required: true
    },
    observacion: {
        type: String,
        required: false
    }    
}

const DetalleComandaSchema = new Schema(DetalleComandaSchemaField);

const DetalleComanda = model<IDetalleComandaDoc>('DetalleComanda', DetalleComandaSchema);

export { DetalleComanda, IDetalleComanda, IDetalleComandaDoc }