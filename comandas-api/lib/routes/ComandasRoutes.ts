import {Request, Response} from 'express';
import {TipoIngredienteController} from '../controllers/TipoIngredienteController'
import {IngredienteController} from '../controllers/IngredienteController'
import {ClienteController} from '../controllers/ClienteController'


export class Routes {
    public tipoIngredienteController: TipoIngredienteController = new TipoIngredienteController();
    public IngredienteController: IngredienteController = new IngredienteController();
    public ClienteController: ClienteController = new ClienteController();


    /**
     * routes
     */
    public routes(app): void {
        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'GET resquest successfull!'
            })
        });

        app.route('/tipoIngrediente')
            .get(this.tipoIngredienteController.getAll)
            .post(this.tipoIngredienteController.create);

        app.route('/tipoIngrediente/:id')
            .get(this.tipoIngredienteController.getOne)
            .put(this.tipoIngredienteController.update)
            .delete(this.tipoIngredienteController.delete);

        app.route('/ingrediente')
            .get(this.IngredienteController.getAll)
            .post(this.IngredienteController.create);

        app.route('/ingrediente/:id')
            .get(this.IngredienteController.getOne)
            .put(this.IngredienteController.update)
            .delete(this.IngredienteController.delete);

        app.route('/cliente')
            .get(this.ClienteController.getAll)
            .post(this.ClienteController.create);

        app.route('/cliente/:id')
            .get(this.ClienteController.getOne)
            .put(this.ClienteController.update)
            .delete(this.ClienteController.delete);
    }
}