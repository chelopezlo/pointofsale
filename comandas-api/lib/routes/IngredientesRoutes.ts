import {Request, Response} from 'express';
import {IngredienteController} from '../controllers/IngredienteController'


export class Routes {
    public IngredienteController: IngredienteController = new IngredienteController();


    /**
     * routes
     */
    public routes(app): void {
        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'GET resquest successfull!'
            })
        });

        app.route('/ingrediente')
            .get(this.IngredienteController.getAll)
            .post(this.IngredienteController.create);

        app.route('/Ingrediente/:id')
            .get(this.IngredienteController.getOne)
            .put(this.IngredienteController.update)
            .delete(this.IngredienteController.delete);
    }
}