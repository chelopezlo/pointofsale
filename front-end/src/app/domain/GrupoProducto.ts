import { IIngrediente } from './Ingrediente';

export interface IProducto {
  _id: string;
  descripcion?: string;
  ingredientes: IIngrediente[]
}

export interface IGrupoProducto{
  _id: string;
  nombre:string;
  descripcion?:string;
  productos?: IProducto[];
}
