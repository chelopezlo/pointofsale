import { ICliente } from './Cliente';
import { IProducto } from './GrupoProducto';


export interface IComanda{
  _id: string;
  resumen: string;
  fecha_agenda: string;
  fecha_entrega: string;
  cliente?: ICliente;
  despacho: boolean;
  valor: number;
  tipoPago: string;
  pagado: boolean;
  productos: IProducto[];
  estados: {
    fecha: string;
    estado: string;
    activo: boolean;
  };
}
