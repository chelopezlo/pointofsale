import { ITipoIngrediente } from './TipoIngrediente';

export interface IIngrediente{
    _id: string;
    nombre: string;
    tipo: string;
    descripcion?: string;
    categoria: string;
    gramaje?: number;
    tipo_ingrediente: ITipoIngrediente;
}

export interface IFrontIngrediente {
  _id: string;
  nombre: string;
  tipo: string;
  descripcion?: string;
  categoria: string;
  gramaje?: number;
}
