export interface ICliente{
  nombre?: string;
  fono?:string;
  direcciones?: {
    direccion:string;
    indicaciones?: string;
  };
}
