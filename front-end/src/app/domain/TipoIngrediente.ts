export interface ITipoIngrediente{
    _id: string;
    nombre:string;
    producto:string;
    descripcion?:string;
}

export interface IFrontTipoIngrediente {
    _id: string;
    nombre: string;
    producto: string;
    descripcion?: string;
}