import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComandaNewComponent } from './components/comanda/new/new.component';
import { IngredienteComponent } from './components/ingrediente/ingrediente.component';
import { LoginComponent } from './components/login/login.component';
import { TiposingredientesComponent } from './components/tiposingredientes/tiposingredientes.component';

const routes: Routes = [
  { path: 'ingrediente', component: IngredienteComponent },
  { path: 'tipoingrediente', component: TiposingredientesComponent },
  { path: 'login', component: LoginComponent },
  { path: 'comanda/new', component: ComandaNewComponent },
  { path: '**', pathMatch: 'full', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
