import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { Observable } from 'rxjs';
import { IFrontTipoIngrediente, ITipoIngrediente } from 'src/app/domain/TipoIngrediente';
import { PointOfSaleService } from 'src/app/services/pointofsale.service';

@Component({
  selector: 'app-tiposingredientes',
  templateUrl: './tiposingredientes.component.html',
  styles: [
    '.p-field{padding-top: 1em;}'
  ]
})
export class TiposingredientesComponent implements OnInit {

  tipoingredienteDialog: boolean = false;

  submitted: boolean = false;

  tipoingredientes: ITipoIngrediente[] = [];

  tipoingrediente: ITipoIngrediente = {} as ITipoIngrediente;

  selectedTipoIngredientes: ITipoIngrediente[] = [] as ITipoIngrediente[];

  constructor(
    private pointOfSaleService : PointOfSaleService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.refreshData();
  }

  refreshData(){
    this.pointOfSaleService.getAllTipoIngrediente()
      .subscribe ( tipoingredientes => {
        this.tipoingredientes = tipoingredientes;
      });
  }

  hideDialog() {
    this.tipoingredienteDialog = false;
    this.submitted = false;
  }

  openNew() {
    this.tipoingrediente = {} as ITipoIngrediente;
    this.submitted = false;
    this.tipoingredienteDialog = true;
  }

  editTipoIngrediente(ingrediente: ITipoIngrediente) {
    this.tipoingrediente = {...ingrediente};
    this.tipoingredienteDialog = true;
    console.log(ingrediente);
  }

  saveTipo() {
    this.submitted = true;
    let serviceResponse: Observable<ITipoIngrediente>;
    let message = {
      detail: "El Tipo de Ingrediente fue guardado con éxito.",
      summary: "Éxito",
      severity: 'success',
      life: 3000
    };
    if (this.tipoingrediente.nombre.trim()) {
        if (this.tipoingrediente._id) {
          let ing: IFrontTipoIngrediente = {
            _id: this.tipoingrediente._id,
            nombre: this.tipoingrediente.nombre,
            producto: this.tipoingrediente.producto,
            descripcion: this.tipoingrediente.descripcion
          }
          serviceResponse = this.pointOfSaleService.updateTipoIngrediente(ing);
          console.log(this.tipoingrediente);
        }
        else {
          serviceResponse = this.pointOfSaleService.addTipoIngrediente(this.tipoingrediente);
        }
        serviceResponse.subscribe(
          (data) => {
          },
          (err: HttpErrorResponse) => {
            message.summary = "Error!";
            message.severity = "error";
            message.detail = "Ha ocurrido un error al guardar el tipo de Ingrediente. Detalles: " + err.message;
              if (err.error instanceof Error) {
                  console.log('Client-side error occured.');
              } else {
                  console.log('Server-side error occured.');
              }
          },
          () => {
            this.messageService.add(message);
            this.refreshData();
          }
        );
        this.tipoingredientes = [...this.tipoingredientes];
        this.tipoingredienteDialog = false;
        this.tipoingrediente = {} as any;
    }
  }

  deleteTipoIngrediente(tipoingrediente: ITipoIngrediente) {
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete ' + tipoingrediente.nombre + '?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          let message = {
            detail: "El Tipo de Ingrediente fue eliminado con éxito.",
            summary: "Éxito",
            severity: 'success',
            life: 3000
          };

          this.pointOfSaleService.deleteTipoIngrediente(tipoingrediente._id).subscribe(
            (data) => {
              console.log(data);
            },
            (err: HttpErrorResponse) => {
              message.detail = "Error al eliminar el tipo de Ingrediente. Detalle: " + err.message;
              message.summary = "Error!";
              message.severity = "error"
              if (err.error instanceof Error) {
                  console.log('Client-side error occured.');
              } else {
                  console.log('Server-side error occured.');
              }
            },
            () => { //complete se ejecuta siempre al terminar la petición
              this.messageService.add(message);
              this.refreshData();
              this.tipoingredienteDialog = false;
              this.tipoingrediente = {} as any;
            }
          );
        }
    });
  }

  deleteSelectedTipoIngredientes() {
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete the selected ingredientes?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          if(this.selectedTipoIngredientes.length > 0)
          {
            let message = {
              detail: "El Ingrediente fue eliminado con éxito.",
              summary: "Éxito",
              severity: 'success',
              life: 3000
            };
            this.selectedTipoIngredientes.forEach( (ing: ITipoIngrediente, index: number, list: ITipoIngrediente[]) => {
              this.pointOfSaleService.deleteTipoIngrediente(ing._id).subscribe(
                (data) => {
                  console.log(data);
                },
                (err: HttpErrorResponse) => {
                  message.detail = "Error al eliminar el Ingrediente. Detalle: " + err.message;
                  message.summary = "Error!";
                  message.severity = "error"
                  if (err.error instanceof Error) {
                      console.log('Client-side error occured.');
                  } else {
                      console.log('Server-side error occured.');
                  }
                },
                () => { //complete se ejecuta siempre al terminar la petición
                  this.messageService.add(message);
                  this.refreshData();
                  this.tipoingredienteDialog = false;
                  this.tipoingrediente = {} as any;
                }
              );
            } )
          }
        }
    });
  }

  filterTable(event: Event, dt: Table, matchType: string){
    dt.filterGlobal((<HTMLInputElement>event.target).value, matchType);
  }

}
