import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { PointOfSaleService } from '../../services/pointofsale.service';
import { IFrontIngrediente, IIngrediente } from '../../domain/Ingrediente';
import { ITipoIngrediente } from '../../domain/TipoIngrediente';
import { isPlatformBrowser } from '@angular/common';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { HttpErrorResponse } from '@angular/common/http';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-ingrediente',
  templateUrl: './ingrediente.component.html',
  styleUrls: ['./ingrediente.component.css'],
  providers: [MessageService,ConfirmationService]
})
export class IngredienteComponent implements OnInit {


  ingredienteDialog: boolean = false;

  ingredientes: IIngrediente[] = [];

  ingrediente: IIngrediente = {} as IIngrediente;

  selectedIngredientes: IIngrediente[] = [] as IIngrediente[];

  submitted: boolean = false;

  tiposIngrediente : ITipoIngrediente[] = [] as ITipoIngrediente[];

  categorias: any[] = [];

  constructor(
    private pointOfSaleService : PointOfSaleService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService) { }

  ngOnInit(): void {
    this.refreshData();
    this.getTiposIngredientes();

    this.categorias = [
      {label: 'BASICO', value: "1"},
      {label: 'PREMIUM', value: "2"},
      {label: 'NA', value: "9"}
    ];
  }

  refreshData(){
    this.pointOfSaleService.getAllIngrediente()
      .subscribe ( ingredientes => {
        this.ingredientes = ingredientes;
      });
  }

  getTiposIngredientes(){
    this.pointOfSaleService.getAllTipoIngrediente()
      .subscribe( tipos => {
        this.tiposIngrediente = tipos;
      });
  }

  openNew() {
    this.ingrediente = {} as IIngrediente;
    this.submitted = false;
    this.ingredienteDialog = true;

  }

  deleteSelectedIngredientes() {
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete the selected ingredientes?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          if(this.selectedIngredientes.length > 0)
          {
            let message = {
              detail: "El Ingrediente fue eliminado con éxito.",
              summary: "Éxito",
              severity: 'success',
              life: 3000
            };
            this.selectedIngredientes.forEach( (ing: IIngrediente, index: number, list: IIngrediente[]) => {
              this.pointOfSaleService.deleteIngrediente(ing._id).subscribe(
                (data) => {
                  console.log(data);
                },
                (err: HttpErrorResponse) => {
                  message.detail = "Error al eliminar el Ingrediente. Detalle: " + err.message;
                  message.summary = "Error!";
                  message.severity = "error"
                  if (err.error instanceof Error) {
                      console.log('Client-side error occured.');
                  } else {
                      console.log('Server-side error occured.');
                  }
                },
                () => { //complete se ejecuta siempre al terminar la petición
                  this.messageService.add(message);
                  this.refreshData();
                  this.ingredienteDialog = false;
                  this.ingrediente = {} as any;
                }
              );
            } )
          }
        }
    });
  }

  editIngrediente(ingrediente: IIngrediente) {
    this.ingrediente = {...ingrediente};
    this.ingredienteDialog = true;
    console.log(ingrediente);
  }

  //filterSearch()

  deleteIngrediente(ingrediente: IIngrediente) {
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete ' + ingrediente.nombre + '?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          let message = {
            detail: "El Ingrediente fue eliminado con éxito.",
            summary: "Éxito",
            severity: 'success',
            life: 3000
          };

          this.pointOfSaleService.deleteIngrediente(ingrediente._id).subscribe(
            (data) => {
              console.log(data);
            },
            (err: HttpErrorResponse) => {
              message.detail = "Error al eliminar el Ingrediente. Detalle: " + err.message;
              message.summary = "Error!";
              message.severity = "error"
              if (err.error instanceof Error) {
                  console.log('Client-side error occured.');
              } else {
                  console.log('Server-side error occured.');
              }
            },
            () => { //complete se ejecuta siempre al terminar la petición
              this.messageService.add(message);
              this.refreshData();
              this.ingredienteDialog = false;
              this.ingrediente = {} as any;
            }
          );
        }
    });
  }

  hideDialog() {
    this.ingredienteDialog = false;
    this.submitted = false;
  }

  saveTipo() {
    this.submitted = true;
    let serviceResponse: Observable<IIngrediente>;
    let message = {
      detail: "El Ingrediente fue guardado con éxito.",
      summary: "Éxito",
      severity: 'success',
      life: 3000
    };
    if (this.ingrediente.nombre.trim()) {
        if (this.ingrediente._id) {
          let ing: IFrontIngrediente = {
            _id: this.ingrediente._id,
            nombre: this.ingrediente.nombre,
            categoria: this.ingrediente.categoria,
            descripcion: this.ingrediente.descripcion,
            gramaje: this.ingrediente.gramaje,
            tipo: this.ingrediente.tipo_ingrediente._id
          }
          serviceResponse = this.pointOfSaleService.updateIngrediente(ing);
          console.log(this.ingrediente);
        }
        else {
          this.ingrediente.tipo = this.ingrediente.tipo_ingrediente._id;
          serviceResponse = this.pointOfSaleService.addIngrediente(this.ingrediente);
        }
        serviceResponse.subscribe(
          (data) => {
          },
          (err: HttpErrorResponse) => {
            message.summary = "Error!";
            message.severity = "error";
            message.detail = "Ha ocurrido un error al guardar el Ingrediente. Detalles: " + err.message;
              if (err.error instanceof Error) {
                  console.log('Client-side error occured.');
              } else {
                  console.log('Server-side error occured.');
              }
          },
          () => {
            this.messageService.add(message);
            this.refreshData();
          }
        );
        this.ingredientes = [...this.ingredientes];
        this.ingredienteDialog = false;
        this.ingrediente = {} as any;
    }
  }

  getCategoria(value: string)
  {
    let cat = this.categorias.filter((item) => item.value == value);
    return cat[0].label;
  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.ingredientes.length; i++) {
        if (this.ingredientes[i]._id === id) {
            index = i;
            break;
        }
    }
    return index;
  }

  filterTable(event: Event, dt: Table, matchType: string){
    dt.filterGlobal((<HTMLInputElement>event.target).value, matchType);
  }


}
