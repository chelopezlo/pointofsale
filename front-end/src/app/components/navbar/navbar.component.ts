import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/api';
import { AuthStateService } from 'src/app/shared/auth-state.service';
import { TokenService } from 'src/app/shared/token.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: [],
})
export class NavbarComponent implements OnInit {
  isSignedIn: boolean = false;
  items: MenuItem[] = [] as MenuItem[];
  constructor(
    private auth: AuthStateService,
    public router: Router,
    public token: TokenService
  ) {}

  ngOnInit(): void {
    this.auth.userAuthState.subscribe((val) => {
      this.isSignedIn = val;
      this.updateMenu();
    });
    console.log(this.isSignedIn);
    this.updateMenu();
  }

  logout() {
    this.auth.setAuthState(false);
    this.token.removeToken();
    this.router.navigate(['login']);
  }

  login()
  {
    this.router.navigate(['login']);
  }

  isLogedIn() {
    return this.isSignedIn;
  }

  updateMenu() {
    this.items = [
      {
        label: 'Inicio',
        icon: 'pi pi-fw pi-home',
        routerLink: ['/'],
        routerLinkActiveOptions: { exact: true },
      },
      {
        label: 'Comandas',
        icon: 'pi pi-fw pi-list',
        visible: this.isLogedIn(),
        items: [
          {
            label: 'Nuevo pedido',
            icon: 'pi pi-fw pi-plus-circle',
            routerLink: ['/comanda/new'],
          },
          {
            label: 'Ver pedidos',
            icon: 'pi pi-fw pi-search',
          },
          {
            label: 'Ver Agenda',
            icon: 'pi pi-fw pi-calendar',
          },
        ],
      },
      {
        label: 'Mantenedor',
        icon: 'pi pi-fw pi-cog',
        visible: this.isLogedIn(),
        items: [
          {
            label: 'Ingredientes',
            icon: 'pi pi-fw pi-pencil',
            routerLink: ['/ingrediente'],
          },
          {
            label: 'Tipo Ingrediente',
            icon: 'pi pi-fw pi-pencil',
            routerLink: ['/tipoingrediente'],
          },
          {
            label: 'Clientes',
            icon: 'pi pi-fw pi-pencil',
          },
          {
            label: 'Login',
            icon: 'pi pi-fw pi-key',
            routerLink: ['/login'],
          },
        ],
      },
    ];
  }
}
