import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MessageService } from 'primeng/api';
import { IComanda } from 'src/app/domain/Comanda';
import { IGrupoProducto, IProducto } from 'src/app/domain/GrupoProducto';
import { IIngrediente } from 'src/app/domain/Ingrediente';
import { ITipoIngrediente } from 'src/app/domain/TipoIngrediente';
import { PointOfSaleService } from 'src/app/services/pointofsale.service';

@Component({
  selector: 'app-comanda-new',
  templateUrl: './new.component.html',
  styles: [
    '.ingrediente-tag {border-radius: 2px; padding: .25em .5rem; text-transform: uppercase; font-weight: 700; font-size: 12px; letter-spacing: .3px;}',
    '.tag {cursor: pointer}'
  ]
})
export class ComandaNewComponent implements OnInit {

  pagado: boolean = false;
  despacho: boolean = false;
  grupos: IGrupoProducto[] = [] as IGrupoProducto[];
  showDialog: boolean = false;
  tiposIngrediente : ITipoIngrediente[] = [] as ITipoIngrediente[];
  ingredientes: IIngrediente[] = [] as IIngrediente[];
  selectedProduct: string = '';
  selectedIngredientes: IIngrediente[] = [] as IIngrediente[];
  productosComanda: IProducto[] = [] as IProducto[];
  SelectedProductos: IProducto[] = [] as IProducto[];
  editedProducto: IProducto = {} as IProducto;

  comanda: IComanda = {} as IComanda;

  constructor(
    private pointOfSaleService : PointOfSaleService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService) {

    this.comanda.productos = [] as IProducto[];
    this.grupos = [
      {
        _id: '1',
        nombre: 'Sushi',
        descripcion: 'Todos los productos relacionados al sushi',
      },
      {
        _id: '2',
        nombre: 'Pizza',
        descripcion: 'Todos los productos relacionados a la pizza',
      },
      {
        _id: '3',
        nombre: 'OTROS',
        descripcion: 'Productos o servicios extras como bebidas o servicio de decorado especial',
      }
    ]
  }

  ngOnInit(): void {
    this.getTiposIngredientes();
    this.getIngredientes();
  }

  showProductDialog(){
    this.showDialog = true;
    console.log(this.showDialog)
  }

  hideDialog(){
    this.showDialog = false;
  }

  cancelAddProduct(){
    this.showDialog = false;
    this.selectedProduct = '';
    this.selectedIngredientes = [] as IIngrediente[];
    this.editedProducto = {} as IProducto;
  }

  getTiposIngredientes(){
    this.pointOfSaleService.getAllTipoIngrediente()
      .subscribe( tipos => {
        this.tiposIngrediente = tipos;
      });
  }

  getIngredientes(){
    this.pointOfSaleService.getAllIngrediente()
      .subscribe ( ingredientes => {
        this.ingredientes = ingredientes;
      });
  }

  filterIngredientesByType(type: string){
    let filteredIngredientes = this.ingredientes.filter((ing: IIngrediente, index: number, arreglo: IIngrediente[]) => {
      return ing.tipo_ingrediente.nombre === type;
    });
    return filteredIngredientes;
  }

  filterTiposByProducto(type: string){
    let filteredTipos = this.tiposIngrediente.filter((item: ITipoIngrediente, index: number, arreglo: ITipoIngrediente[]) => {
      return item.producto === type;
    });
    return filteredTipos;
  }

  addIngrediente(ingrediente: IIngrediente){
    console.log(ingrediente);
    if(this.selectedIngredientes.some( (ing:IIngrediente) => {return ing._id === ingrediente._id}))
    {
      return;
    }
    else{
      this.selectedIngredientes.push(ingrediente);
    }
    console.log(this.selectedIngredientes);
  }

  removeIngrediente(ingrediente: IIngrediente){
    if(this.selectedIngredientes.indexOf(ingrediente) < 0)
    {
      return;
    }
    else{
      this.selectedIngredientes = this.selectedIngredientes.filter(ing => ing._id != ingrediente._id);
    }
    console.log(this.selectedIngredientes);
  }

  addProducto(){
    if(this.selectedIngredientes.length === 0){
      return;
    }
    else{
      this.editedProducto.ingredientes = this.selectedIngredientes;
      this.comanda.productos.push(this.editedProducto);
      this.cancelAddProduct();
    }
    console.log(this.comanda);
  }

  removeProducto(producto: IProducto){
    if(this.productosComanda.indexOf(producto) < 0)
    {
      return;
    }
    else{
      this.productosComanda = this.productosComanda.filter(prod => prod._id != producto._id);
    }
    console.log(this.selectedIngredientes);
  }

  editProducto(producto: IProducto){
    if(this.productosComanda.indexOf(producto) < 0)
    {
      return;
    }
    else{
      this.productosComanda = this.productosComanda.filter(prod => prod._id != producto._id);
    }
    console.log(this.selectedIngredientes);
  }

}
