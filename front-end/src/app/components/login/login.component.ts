import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AuthStateService } from 'src/app/shared/auth-state.service';
import { AuthService, User } from 'src/app/shared/auth.service';
import { TokenService } from 'src/app/shared/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: ['.p-field { padding-top: 1em; }'],
  providers: [MessageService, ConfirmationService],
})
export class LoginComponent implements OnInit {
  usuario: any = {
    username: '',
    password: '',
    recordar: false,
  };

  success: boolean = false;
  submitted: boolean = false;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    public authService: AuthService,
    public router: Router,
    private token: TokenService,
    private authState: AuthStateService
  ) {}

  ngOnInit(): void {}

  resetForm()
  {
    this.usuario = {
      username: '',
      password: '',
      recordar: false
    };
  }

  doLogin() {
    let user: User = {
      email: this.usuario.username,
      password: this.usuario.password,
      name: '',
      password_confirmation: '',
    };

    let message = {
      detail: 'Ingreso exitoso.',
      summary: 'Éxito',
      severity: 'success',
      life: 3000,
    };

    this.authService.signin(user).subscribe(
      (result) => {
        this.success = true;
        this.responseHandler(result);
      },
      (err: HttpErrorResponse) => {
        this.success = false;
        message.detail =
          'Error al ingresar. Detalle: ' + err.message;
        message.summary = 'Error!';
        message.severity = 'error';
        if (err.error instanceof Error) {
          console.log('Client-side error occured.');
        } else {
          console.log('Server-side error occured.');
        }
      },
      () => {
        this.messageService.add(message);
        this.authState.setAuthState(true);
        this.resetForm();
      }
    );
  }

  responseHandler(data: any) {
    this.token.handleData(data.access_token);
  }

  onClose(){
    if(this.success){
      this.router.navigate(['/']);
    }
  }
}
