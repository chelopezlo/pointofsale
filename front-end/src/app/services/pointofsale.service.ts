import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";

import {  Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { ITipoIngrediente } from '../domain/TipoIngrediente';
import { IIngrediente, IFrontIngrediente } from '../domain/Ingrediente';

@Injectable({
  providedIn: 'root'
})
export class PointOfSaleService {

  private endpoint = 'http://localhost:8000/api';

  constructor( private http: HttpClient) { }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    console.log(errorMessage);
    return throwError(errorMessage);
  }

  //Ingrediente

  getAllIngrediente() : Observable<IIngrediente[]>{
    return this.http.get<IIngrediente[]>( `${this.endpoint}/ingrediente` );
  }

  getOneIngrediente(ingredienteId: string) : Observable<IIngrediente>{
    return this.http.get<IIngrediente>(`${this.endpoint}/ingrediente/${ingredienteId}`);
  }

  addIngrediente(ingrediente: IIngrediente) : Observable<IIngrediente>{
    const {_id, ...ing} = ingrediente;
    console.log(ing);

    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post<IIngrediente>( `${this.endpoint}/ingrediente`, ing, {headers: headers} ).pipe(catchError(this.handleError));
  }

  updateIngrediente(ingrediente: IFrontIngrediente) : Observable<IIngrediente>{
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.put<IIngrediente>(`${this.endpoint}/ingrediente/${ingrediente._id}`, ingrediente, {headers: headers});
  }

  deleteIngrediente(ingredienteId: string) : Observable<IIngrediente>{
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.delete<IIngrediente>(`${this.endpoint}/ingrediente/${ingredienteId}`, {headers: headers});
  }

  //Tipo Ingrediente

  getAllTipoIngrediente() : Observable<ITipoIngrediente[]>{
    return this.http.get<ITipoIngrediente[]>( `${this.endpoint}/tipo` );
  }

  getOneTipoIngrediente(tipoIngredienteId: string) : Observable<ITipoIngrediente>{
    return this.http.get<ITipoIngrediente>(`${this.endpoint}/tipo/${tipoIngredienteId}`);
  }

  addTipoIngrediente(tipoIngrediente: ITipoIngrediente) : Observable<ITipoIngrediente>{
    const {_id, ...ing} = tipoIngrediente;
    console.log(ing);

    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.post<ITipoIngrediente>( `${this.endpoint}/tipo`, ing, {headers: headers} ).pipe(catchError(this.handleError));
  }

  updateTipoIngrediente(tipoIngrediente: ITipoIngrediente) : Observable<ITipoIngrediente>{
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.put<ITipoIngrediente>(`${this.endpoint}/tipo/${tipoIngrediente._id}`, tipoIngrediente, {headers: headers});
  }

  deleteTipoIngrediente(tipoIngredienteId: string) : Observable<ITipoIngrediente>{
    const headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json; charset=utf-8');

    return this.http.delete<ITipoIngrediente>(`${this.endpoint}/tipo/${tipoIngredienteId}`, {headers: headers});
  }
}
